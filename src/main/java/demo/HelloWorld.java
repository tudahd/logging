package demo;

import org.apache.log4j.Logger;

/**
 * Created by Anh Tu on 10/5/2017.
 */
public class HelloWorld{

    final static Logger logger = Logger.getLogger(HelloWorld.class);

    public static void main(String[] args) {

        HelloWorld obj = new HelloWorld();

        try{
            obj.divide();
        }catch(ArithmeticException ex){
            logger.error("Sorry, something wrong!", ex);
        }


    }

    private void divide(){

        int i = 10 /0;

    }

}
