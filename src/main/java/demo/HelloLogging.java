package demo;

import org.apache.log4j.Logger;

/**
 * Created by Anh Tu on 10/5/2017.
 */
public class HelloLogging {
    final static Logger logger = Logger.getLogger(HelloLogging.class);

    public static void main(String[] args) {

        HelloLogging obj = new HelloLogging();
        obj.runMe("hello");

    }

    private void runMe(String parameter){

        if(logger.isDebugEnabled()){
            logger.debug("This is debug : " + parameter);
        }

        if(logger.isInfoEnabled()){
            logger.info("This is info : " + parameter);
        }

        logger.warn("This is warn : " + parameter);
        logger.error("This is error : " + parameter);
        logger.fatal("This is fatal : " + parameter);

    }

}

